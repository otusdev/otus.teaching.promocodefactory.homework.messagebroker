﻿using Otus.Teaching.Pcf.ReceivingFromPartner.Core.Abstractions.Gateways;
using Otus.Teaching.Pcf.ReceivingFromPartner.Integration.Dto;
using RabbitMQ.Client;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Otus.Teaching.Pcf.ReceivingFromPartner.Core.Domain;
using System.Text.Json;
using Microsoft.Extensions.Options;
using Otus.Teaching.Pcf.SharedClasses;

namespace Otus.Teaching.Pcf.ReceivingFromPartner.Integration
{
    public class NotificationGatewayRabbitMq : IAdministrationGateway, IGivingPromoCodeToCustomerGateway
    {
        private readonly string _exchange = "ReceivingFromPartner.Exchange";
        private const string RoutingKeyAdministrationGateway = "AdministrationGateway";
        private const string RoutingKeyGivingPromoCodeToCustomerGateway = "GivingPromoCodeToCustomerGateway";
        private readonly ConnectionFactory _connectionFactory;

        public NotificationGatewayRabbitMq(IOptions<RabbitMqSettings> options)
        {            
            _connectionFactory = new()
            {
                UserName = options.Value.User,
                Password = options.Value.Password,
                Port = options.Value.Port,
                HostName = options.Value.Server,
                VirtualHost = options.Value.VirtualHost
            };
            using var con = _connectionFactory.CreateConnection();
            if (con.IsOpen)
            {
                using var channel = con.CreateModel();              
                channel.ExchangeDeclare(
                    exchange: _exchange,
                    type: ExchangeType.Direct,
                    durable: true);
            }
        }

        public Task GivePromoCodeToCustomer(PromoCode promoCode)
        {
            var dto = new GivePromoCodeToCustomerDto()
            {
                PartnerId = promoCode.Partner.Id,
                BeginDate = promoCode.BeginDate.ToShortDateString(),
                EndDate = promoCode.EndDate.ToShortDateString(),
                PreferenceId = promoCode.PreferenceId,
                PromoCode = promoCode.Code,
                ServiceInfo = promoCode.ServiceInfo,
                PartnerManagerId = promoCode.PartnerManagerId
            };
            using var con = _connectionFactory.CreateConnection();
            if (con.IsOpen)
            {
                using var channel = con.CreateModel();
                var bytes = Encoding.Unicode.GetBytes(JsonSerializer.Serialize(dto));
                channel.BasicPublish(
                    exchange: _exchange,
                    routingKey: RoutingKeyGivingPromoCodeToCustomerGateway,
                    body: bytes);
            }
            return Task.CompletedTask;
        }

        public Task NotifyAdminAboutPartnerManagerPromoCode(Guid partnerManagerId)
        {
            using var con = _connectionFactory.CreateConnection();
            if (con.IsOpen)
            {
                using var channel = con.CreateModel();
                var bytes = Encoding.UTF8.GetBytes(partnerManagerId.ToString());               
                channel.BasicPublish(
                    exchange: _exchange,
                    routingKey: RoutingKeyAdministrationGateway,
                    body: bytes);
            }
            return Task.CompletedTask;
        }
    }
}

