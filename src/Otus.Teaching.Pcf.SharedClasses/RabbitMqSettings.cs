﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Otus.Teaching.Pcf.SharedClasses
{
    public class RabbitMqSettings
    {
        public string User { get; set; }
        public string Password { get; set; }
        public string Server { get; set; }
        public string VirtualHost { get; set; }
        public int Port { get; set; }

    }
}
